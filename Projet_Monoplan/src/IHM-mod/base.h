#include <gtk/gtk.h>
#include "../Graph-mod/graph.h"

typedef struct bind
{
    // position dans la feuille 
    int ligne, colonne;
    GtkWidget *theCase;
    s_cell *cell;
} s_bind;

s_bind *bindLookupByCell(s_cell *cell);
s_bind *bindLookupByPos(int colonne, int ligne);