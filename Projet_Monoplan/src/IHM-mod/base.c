#include "./base.h"

struct interface { 
	GtkWindow *root, *aPropos; // les deux fenêtres 
	GtkWidget *entry; // le champ où on saisit la formule 
	GtkWidget *viewport; // le viewport qui permet de “voir” les cellules 
	GtkWidget *filename; // le nom du fichier 
	GtkWidget *fchooser; // le nom du fichier 
	GtkWidget *grid; // le gridau avec cells 
	GtkWidget *like; // le gridau avec cells 
} ihm;


#define MAX_ALL MAX_COL * MAX_ROW
s_bind *binds;

s_bind * bindLookupByCell(s_cell *cell)
{
		return NULL;
}
s_bind * bindLookupByPos(int colonne, int ligne)
{
	return NULL;

}

void App_Quit(GtkWidget *pWidget, gpointer pData)
{
    /* Arret de la boucle évènementielle */
    gtk_main_quit();
}

void Show_Aide(GtkWidget *pWidget, gpointer pData)
{
    /* Arret de la boucle évènementielle */
    gtk_widget_show((GtkWidget *)ihm.aPropos);
}
/*
void modifyFormula(GtkWidget *pWidget, gpointer pData)
{
	gchar *text=(gchar *)gtk_entry_get_text(GTK_ENTRY((GtkWidget **)pData));
	gtk_label_set_text(ihm.entry, text);
}
*/
void Show_FChooser(GtkWidget *pWidget, gpointer pData)
{
	gtk_widget_show(ihm.fchooser);
}

void Destroy_FChooser(GtkWidget *pWidget, gpointer pData)
{
	gtk_widget_hide(ihm.fchooser);
}

void show_like(GtkWidget *pWidget, gpointer pData)
{
	gtk_widget_show(ihm.like);
}

void close_like(GtkWidget *pWidget, gpointer pData)
{
	gtk_widget_hide(ihm.like);
}

void New_Sheet(GtkWidget *pWidget, gpointer pData)
{
	gtk_label_set_text (ihm.filename, "*myNewSheet.gg");
	ihm.grid = (GtkWidget*)gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(ihm.viewport), GTK_GRID(ihm.grid));

	for (int col = 0; col < MAX_ROW + 1; col++)
		for (int row = 0; row < MAX_COL + 1; row++)
			if (col == 0 && row == 0)
			{
				gtk_grid_attach(GTK_GRID(ihm.grid), gtk_label_new(""), row*10, col*7, 10, 7);
			}
			else if (col == 0) 
			{
				char b[3];
				sprintf (b, "%c", row + 64);
				gtk_grid_attach(GTK_GRID(ihm.grid), gtk_label_new(b), row*10, col*7, 10, 7);
			}
			else if (row == 0) 
			{
				char b[3];
				sprintf (b, "%d", col);
				gtk_grid_attach(GTK_GRID(ihm.grid), gtk_label_new(b), row*10, col*7, 10, 7);
			}
			else 
			{
				GtkWidget *child = gtk_entry_new();
				//g_signal_connect(G_OBJECT(child), "changed", G_CALLBACK(modifyFormula), NULL);
				gtk_grid_attach(GTK_GRID(ihm.grid), child, row*10, col*7, 10, 7);
			}

	gtk_widget_show_all(ihm.grid);
}

int main(int argc, char *argv[]) {
	gtk_init(&argc, &argv);
	GtkBuilder *builder;
	GError *error = NULL;
	GObject *button;
	
	// Import the file
	builder = gtk_builder_new();
	if (gtk_builder_add_from_file(builder, "exe/ggmonoplan.glade", &error) == 0)
    {
    	g_printerr ("Error loading file: %s\n", error->message);
		g_clear_error (&error);
    	return 1;
    }

	// Setting the ihm things
	gtk_builder_connect_signals(builder, &ihm);
	ihm.root = GTK_WINDOW(gtk_builder_get_object(builder, "rootWindow"));
	ihm.aPropos = GTK_WINDOW(gtk_builder_get_object(builder, "aPropos"));
	ihm.viewport = GTK_WIDGET(gtk_builder_get_object(builder, "viewport1"));
	ihm.entry = GTK_WIDGET(gtk_builder_get_object(builder, "entry1"));
	ihm.filename = GTK_WIDGET(gtk_builder_get_object(builder, "nomFichier"));
	ihm.fchooser = GTK_WIDGET(gtk_builder_get_object(builder, "fchooser"));
	ihm.like = GTK_WIDGET(gtk_builder_get_object(builder, "likePage"));

	// create a new sheet	

	//Launch the window
	gtk_widget_show_all((GtkWidget *)ihm.root);
	gtk_main();
	return 0;
}
