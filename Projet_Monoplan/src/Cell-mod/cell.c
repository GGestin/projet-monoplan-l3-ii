#include "cell.h"

// La fonction qui analyse la chaîne de caracteres
// associee a une cellule (le contenu de la cellule)
// Notation Polonaise Inversé :  3 5 + 4 * C42 /  --> ((3+5)*4)/C42
void analyseCellString(s_sheets *sh, s_cell *theCell)
{
	if ((sh == NULL) || (theCell == NULL))
		return;
	
	char *data = strdup(theCell->valueString);
	if ((data == NULL) || (strlen(data) <= 0))
		return;

	const char *delim = " ";
	char *strToken = strtok(data, delim);
	node_t *formula = list_create();

	if (strcmp(strToken, "=") != 0)
	{
		int notANumber = 0;
		int point = 0;
		int i = 0;

		if (theCell->valueString[0] == '-') i++;
		// Test if only '-' optional at begining numbers and only 1 '.'
		while ((i < strlen(theCell->valueString)) && (notANumber == 0) && (point <= 1))
		{
			if (!isdigit(theCell->valueString[i]) && (theCell->valueString[i] != '.')) 
				notANumber = 1;
			if (theCell->valueString[i] == '.')
				point++;
			i++;
		}

		if (notANumber || point > 1)
			theCell->valueNumber = 0.0;
		else
			theCell->valueNumber = strtod(theCell->valueString, NULL);
	}
	else
	{
		strToken = strtok(NULL, delim);
		
		while (strToken != NULL)
		{
			s_token *tok = (s_token *)malloc(sizeof(s_token));
			int notANumber = 0;
			int point = 0;
			int moins = 0;
			int i = 0;

			// Case Number
			if ((strToken[0] == '-') && (strlen(strToken) >= 2)) i++;
			while ((i < strlen(strToken)) && (notANumber == 0) && (point <= 1))
			{
				if (!isdigit(strToken[i]) && (strToken[i] != '.')) 
					notANumber = 1;
				if (strToken[i] == '.')
					point++;
				i++;
			}

			if (!notANumber && point <= 1)
			{
				tok->type = VALUE;
				tok->value.cst = strtod(strToken, NULL);	
			}

			// Case Ref
			else if (isalpha(strToken[0]) && (strlen(strToken) <= 3) && (isdigit(strToken[1])))
			{
				// test si on du max (marche pas le 27/11)
				// char *ref = strdup(strToken);
				// char *del;
				// strncat(del, ref+1, strlen(ref));

				// int adr = strtod(del, NULL);
				// if (adr > MAX_ROW)
				// 	adr = MAX_ROW;

				if (theCell->areDependent == NULL)
					theCell->areDependent = list_create();

				s_cell *refCell = getCell(sh, strToken);
				// SUCCECEURS AJOUTE
				refCell->areDependent = list_insert(refCell->areDependent, theCell);

				tok->type = REF;
				tok->value.ref = refCell;
			}

			// Case Operator
			else
			{
				int i = 0;
				while ((i < NB_OP) && (strcmp(strToken, tabOp[i].nom) != 0)) i++;

				if (strcmp(strToken, tabOp[i].nom) == 0)
				{
					tok->type = OPERATOR;
					tok->value.operator = tabOp[i].operator;
				}
				else
				{
					printf("Error : ", strToken);
				}
				
			}
			formula = list_append(formula, tok);
			strToken = strtok(NULL, delim);
		}
	}
	theCell->formula = formula;
	theCell->negDre = 0;

	if (sh->cells == NULL)
		sh->cells = list_create();

	free(data);
}

s_cell *getCell(s_sheets *sh, char *cName)
{
	if (cName == NULL || strlen(cName) <= 0)
		return NULL;

	node_t *tmp, *p, *head = sh->cells;
	if (head != NULL)
	{
		if (strcmp(((s_cell *)list_get_data(head))->id, cName) == 0)
			return list_get_data(head);

		else
		{
			tmp = head;
			while ((list_next(tmp) != NULL) && (strcmp(((s_cell *)list_get_data(list_next(tmp)))->id, cName) != 0))
				tmp = list_next(tmp);

			if (list_next(tmp) != NULL)
			{
				return list_get_data(list_next(tmp));
			}
			else
			{
				s_cell *newCell = (s_cell *)malloc(sizeof(s_cell));
				newCell->id = cName;
				head = list_append(head, newCell);
				return newCell;
			}
		}
	}
	else
	{
		s_cell *newCell = (s_cell *)malloc(sizeof(s_cell));
		newCell->id = cName;
		head = list_append(head, newCell);
		return newCell;
	}
}

// La fonction qui evalue une cellule unique
void evaluateCell(s_cell *theCell)
{
	if (theCell == NULL) return;
	if (theCell->formula == NULL)
		return;

	node_t *head = theCell->formula;
	my_stack_t *st = STACK_CREATE(MAX_STACK, double);
	s_token *tok;

	while (head != NULL) {
		tok =((s_token*)list_get_data(head));

		switch (tok->type)
		{
		case VALUE:
			STACK_PUSH(st, tok->value.cst, double);
			break;
		
		case REF:
			STACK_PUSH(st, tok->value.ref->valueNumber, double);
			break;

		case OPERATOR:
			(*(tok->value.operator))(st);
			break;
		}
		head = list_next(head);
	}

	if (!STACK_VALUE_AVAILABLE(st))
		theCell->valueNumber = 0;
	else
		theCell->valueNumber = STACK_POP(st, double);
	
	free(st);
	st = NULL;
}
