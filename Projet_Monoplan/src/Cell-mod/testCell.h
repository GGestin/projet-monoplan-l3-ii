#include <stdio.h>
#include <stdlib.h>
#include "cell.h"

int runTestCell();
int test_add();
int test_substract();
int test_multiply();
int test_divide();
int test_modulo();
int test_min();
int test_max();
int test_analyseCellString();
int test_evaluateCell();