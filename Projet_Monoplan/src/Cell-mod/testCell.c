#include "testCell.h"

int runTestCell()
{
	printf("\n<-------------------->");
	printf("\n<- BEGIN testCell.c ->");
	printf("\n<-------------------->\n");

	int ret = 0;
    ret += test_add();
    ret += test_substract();
    ret += test_multiply();
    ret += test_divide();
    ret += test_modulo();
    ret += test_min();
    ret += test_max();
    ret += test_analyseCellString();
    ret += test_evaluateCell();

	printf("\n%d : Errors in tests ", ret);
	printf("\n\n<-------------------->");
	printf("\n<-- END testCell.c -->");
	printf("\n<-------------------->");
	return ret;
}

int test_add()
{
    printf("\n-----> test_add()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 21 21 +";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 42) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "= 0 0 +";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);
    if(Cell0->valueNumber != 0) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= -1 -1 +";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);
    if(Cell1->valueNumber != -2) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_substract()
{
    printf("\n-----> test_substract()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 1 1 -";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 0) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "= 64 22 -";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);

    if(Cell0->valueNumber != 42) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= 0 1 -";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);

    if(Cell1->valueNumber != -1) ret++;

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= 0 -1 -";
    Cell2->id = "C2"; 
    analyseCellString(sheet, Cell2);
    evaluateCell(Cell2);

    if(Cell2->valueNumber != 1) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_multiply()
{
    printf("\n-----> test_multiply()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 21 2 *";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 42) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "= 0 0 *";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);
    if(Cell0->valueNumber != 0) ret++;

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= 42 0 *";
    Cell2->id = "C0"; 
    analyseCellString(sheet, Cell2);
    evaluateCell(Cell2);
    if(Cell2->valueNumber != 0) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= -1 -1 *";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);
    if(Cell1->valueNumber != 1) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_divide()
{
    printf("\n-----> test_divide()");
		int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 84 2 /";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 42) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "= 0 0 /";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);
    if(Cell0->valueNumber != 0) ret++;

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= 10 5 /";
    Cell2->id = "C0"; 
    analyseCellString(sheet, Cell2);
    evaluateCell(Cell2);
    if(Cell2->valueNumber != 2) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= 10 -1 /";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);
    if(Cell1->valueNumber != -10) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_modulo()
{
    printf("\n-----> test_modulo()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 42 2 mod";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 0) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "= 3 4 mod";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);
    if(Cell0->valueNumber != 3) ret++;

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= 13 4 mod";
    Cell2->id = "C0"; 
    analyseCellString(sheet, Cell2);
    evaluateCell(Cell2);
    if(Cell2->valueNumber != 1) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= 124 82 mod";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);
    if(Cell1->valueNumber != 42) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_min(){
    
    printf("\n-----> test_min()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 69 42 min";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 42) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "25";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= C42 C0 min";
    Cell2->id = "C2"; 
    analyseCellString(sheet, Cell2);
    evaluateCell(Cell2);
    if(Cell2->valueNumber != 25) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= 1 2 + 4 min";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);
    if(Cell1->valueNumber != 3) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_max(){
    
    printf("\n-----> test_max()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 21 42 max";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 42) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "25";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= C42 C0 max";
    Cell2->id = "C2"; 
    analyseCellString(sheet, Cell2);
    evaluateCell(Cell2);
    if(Cell2->valueNumber != 42) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= 1 2 + 4 max";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);
    if(Cell1->valueNumber != 4) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_analyseCellString()
{
    printf("\n-----> test_analyseCellString()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "42";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    if(strcmp(Cell42->valueString, "42")) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "Hello world !";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    if(strcmp(Cell0->valueString, "Hello world !")) ret++;

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "";
    Cell2->id = "C0"; 
    analyseCellString(sheet, Cell2);
    if(strcmp(Cell2->valueString, "")) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= 42 2 *";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    if(strcmp(Cell1->valueString, "= 42 2 *")) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_evaluateCell()
{
    printf("\n-----> test_evaluateCell()");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "42";
    Cell42->id = "C42"; 
    analyseCellString(sheet, Cell42);
    evaluateCell(Cell42);
    if(Cell42->valueNumber != 42) ret++;

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "= 2 10 *";
    Cell0->id = "C0"; 
    analyseCellString(sheet, Cell0);
    evaluateCell(Cell0);
    if(Cell0->valueNumber > 20.1 || Cell0->valueNumber < 19.9) ret++;

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= C0";
    Cell2->id = "C2"; 
    analyseCellString(sheet, Cell2);
    evaluateCell(Cell2);
    if(Cell2->valueNumber > 20.1 || Cell2->valueNumber < 19.9) ret++;

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "= C0 C2 *";
    Cell1->id = "C1"; 
    analyseCellString(sheet, Cell1);
    evaluateCell(Cell1);
    if(Cell1->valueNumber > 400.1 || Cell1->valueNumber < 399.9) ret++;

    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
	printf("\t-> %d Errors", ret);
	return ret;
}
