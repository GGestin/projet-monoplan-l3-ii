#include "../stack-lib/stack.h"
#include "../List-mod/list.h"
#include "./operateurs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

// Limite definie dans le pdf
#define MAX_COL 26
#define MAX_ROW 50
#define MAX_STACK 64

typedef struct cell s_cell;
typedef struct sheets s_sheets;
typedef struct token s_token;

// Au niveau de la structure de donnees, une cellule est definie par les informations suivantes (type s_cell) :
// — la chaine de caracteres que l’utilisateur a saisi lors de l’edition de la cellule,
// — la valeur numerique de type double associee a la cellule (ou 0.0 si celle-ci n’est pas calculable),
// — la liste des jetons construite a partir de l’analyse de la formule (voir ci-dessous),
// — la liste des cellules qui dependent de la cellule consideree.
// Une cellule existe dans la structure de donnees lorsque son contenu n’est pas vide et/ou lorsqu’elle est referencee par au moins une autre cellule.
struct cell
{
	char *id;
	char *valueString;
	double valueNumber;
	node_t *formula;
	node_t *areDependent;
	int negDre;
};

// A ce point du projet, les informations qui decrivent la feuille de calcul sont :
// — le nom du fichier associe pour l’enregistrement de la feuille,
// — le nombre de lignes et de colonnes,
// — la liste des cellules existantes.
// Ces informations seront enregistrees dans une variable externe (c’est a dire globale).
struct sheets
{
	char *filename;
	int row;
	int col;
	node_t *cells;
};

// Apres l’edition d’une cellule, le nouveau contenu est decompose en une liste de jetons (token).
// Un jeton represente soit un nombre, soit une reference vers une autre cellule, soit un operateur.
// Un jeton est decrit par la structure de donnees suivante :
// cst enregistre une constante,
// ref un pointeur vers une autre cellule (le type s_cell est une description de cellule),
// operator un pointeur vers une fonction qui implante un operateur.
// L’ensemble des operations disponibles est enregistre dans un tableau de classe externe.
// Une operation est definie par un pointeur sur la fonction qui l’effectue, et un nom (une chaıne de caracteres) qui la designe
struct token
{
	enum
	{
		VALUE,
		REF,
		OPERATOR
	} type;

	union
	{
		double cst;
		s_cell *ref;
		void (*operator)(my_stack_t *eval);
	} value;
};

// La fonction qui cherche une cell grace à son ID
// Si elle ne trouve rien elle creer la s_cell
s_cell *getCell(s_sheets *sh, char *cName);

// La fonction qui analyse la chaîne de caracteres
// associee a une cellule (le contenu de la cellule)
void analyseCellString(s_sheets *sh, s_cell *theCell);

// La fonction qui evalue une cellule unique
void evaluateCell(s_cell *theCell);