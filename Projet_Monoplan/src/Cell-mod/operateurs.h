#include "../stack-lib/stack.h"
#include <string.h>
#include <math.h>

#define NB_OP 7

typedef struct operateur
{
    const char* nom;
    void (*operator)(my_stack_t *eval);
} s_operateur;

extern s_operateur tabOp[];

void add(my_stack_t *st);
void substract(my_stack_t *st);
void multiply(my_stack_t *st);
void divide(my_stack_t *st);
void modulo(my_stack_t *st);
void min(my_stack_t *st);
void max(my_stack_t *st);