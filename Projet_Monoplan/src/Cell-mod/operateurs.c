#include "./operateurs.h"

s_operateur tabOp[NB_OP] = {
    {"+", add},
    {"-", substract},
    {"*", multiply},
    {"/", divide},
    {"mod", modulo},
    {"min", min},
    {"max", max}
};

void add(my_stack_t *st)
{
	double val1 = STACK_POP(st, double);
	double val2 = STACK_POP(st, double);
	STACK_PUSH(st, val1 + val2, double);
}

void substract(my_stack_t *st)
{
	double val1 = STACK_POP(st, double);
	double val2 = STACK_POP(st, double);
	STACK_PUSH(st, val2 - val1, double);
}

void multiply(my_stack_t *st)
{
	double val1 = STACK_POP(st, double);
	double val2 = STACK_POP(st, double);
	STACK_PUSH(st, val1 * val2, double);
}

void divide(my_stack_t *st)
{
	double val1 = STACK_POP(st, double);
	double val2 = STACK_POP(st, double);
	if(val2 == 0) STACK_PUSH(st, 0, double);
	else STACK_PUSH(st, val2 / val1, double);
}

void modulo(my_stack_t *st)
{
	double val1 = STACK_POP(st, double);
	double val2 = STACK_POP(st, double);
	if(val2 == 0) STACK_PUSH(st, 0, double);
	else STACK_PUSH(st, fmod(val2, val1), double);
}

void min(my_stack_t *st)
{
	double val1 = STACK_POP(st, double);
	double val2 = STACK_POP(st, double);
    if (val2 < val1) val1 = val2;
	STACK_PUSH(st, val1, double);
}

void max(my_stack_t *st)
{
	double val1 = STACK_POP(st, double);
	double val2 = STACK_POP(st, double);
    if (val2 > val1) val1 = val2;
	STACK_PUSH(st, val1, double);
}