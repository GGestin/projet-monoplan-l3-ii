#include "./graph.h"

void triTopologique(s_cell *Sinit)
{
    if (Sinit == NULL) return;

    // Setup negDegree in the graph
    recurseDegree(Sinit);   

    // Setup the sub_graph with only concerned nodes
    node_t *subGraph = list_create();
    subGraph = list_insert(subGraph, Sinit);

    while (subGraph != NULL)
    {
        s_cell *c = (s_cell*)list_get_data(subGraph);

        evaluateCell(c);
        subGraph = list_remove(subGraph, c);

        node_t *succ = c->areDependent;
        s_cell *next = (s_cell*)list_get_data(succ);

        while (succ != NULL)
        {
            next->negDre--;
            if (next->negDre == 0)
            {
                subGraph = list_insert(subGraph, next);
                succ = list_next(succ);
            }
            next = (s_cell*)list_get_data(succ);
        }
    }
    list_destroy(subGraph);
}

void recurseDegree(s_cell *theCell)
{
    if (theCell == NULL) return;
    
    node_t *list = theCell->areDependent;

    while (list != NULL)
    {
        s_cell *succCell = (s_cell*)list_get_data(list);
        succCell->negDre = theCell->negDre + 1;
        recurseDegree(succCell);
        list = list_next(list);
    }
}