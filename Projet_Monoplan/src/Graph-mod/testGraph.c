#include "./testGraph.h"

int main()
{
    printf("\n-----> test_Graph()\n");
	int ret = 0;
	s_sheets *sheet = (s_sheets *)malloc(sizeof(s_sheets));
    sheet->filename = "myShit.gg";
    sheet->col = MAX_COL;
    sheet->row = MAX_ROW;

    s_cell *Cell42 = (s_cell *)malloc(sizeof(s_cell));
    Cell42->valueString = "= 21 2 *";
    Cell42->id = "C42"; 
    sheet->cells = list_insert(sheet->cells, Cell42);
    analyseCellString(sheet, Cell42);
    triTopologique(Cell42);

    s_cell *Cell1 = (s_cell *)malloc(sizeof(s_cell));
    Cell1->valueString = "2";
    Cell1->id = "C1"; 
    sheet->cells = list_insert(sheet->cells, Cell1);
    analyseCellString(sheet, Cell1);
    triTopologique(Cell1);

    s_cell *Cell0 = (s_cell *)malloc(sizeof(s_cell));
    Cell0->valueString = "= C42 C1 *";
    Cell0->id = "C0"; 
    sheet->cells = list_insert(sheet->cells, Cell0);
    analyseCellString(sheet, Cell0);
    triTopologique(Cell0);

    s_cell *Cell2 = (s_cell *)malloc(sizeof(s_cell));
    Cell2->valueString = "= C0 5 min";
    Cell2->id = "C2"; 
    sheet->cells = list_insert(sheet->cells, Cell2);

    analyseCellString(sheet, Cell2);
    triTopologique(Cell2);
    
    printf("\nnodes {C42, C0, C1, C2}\n");
    printf("degrees {%d, %d, %d, %d}\n", Cell42->negDre, Cell0->negDre, Cell1->negDre, Cell2->negDre);
    printf("values {%lf, %lf, %lf, %lf}\n", Cell42->valueNumber, Cell0->valueNumber, Cell1->valueNumber, Cell2->valueNumber);
    printf("Reponses {42, 84, 2, 5}\n");


    Cell42->valueString = "= 0 2 *";
    analyseCellString(sheet, Cell42);
    triTopologique(Cell42);
    printf("\nC42 valS: %s\n", Cell42->valueString);
    printf("C42 valN: %lf\n", Cell42->valueNumber);

    printf("\nnodes {C42, C0, C1, C2}\n");
    printf("degrees {%d, %d, %d, %d}\n", Cell42->negDre, Cell0->negDre, Cell1->negDre, Cell2->negDre);
    printf("values {%lf, %lf, %lf, %lf}\n", Cell42->valueNumber, Cell0->valueNumber, Cell1->valueNumber, Cell2->valueNumber);
    printf("Reponses {0, 0, 2, 0}\n");
    
    free(sheet);
    free(Cell42);
    free(Cell0);
    free(Cell1);
    free(Cell2);
    
    return 0;
}
