#include <stdio.h>
#include <stdlib.h>

#ifndef NODE_H
#define NODE_H
typedef struct node node_t;

struct node {
	void *data_node;
	node_t *next_node;
};
#endif
// creation d'une nouvelle liste vide
node_t *list_create(void);

void *list_get_data(node_t *node);

// lire ou ecrire la donnee d'un noeud
void list_set_data(node_t *node, void *data);

// obtenir le noeud suivant
node_t *list_next(node_t *node);

// creation et insertion d'un noeud en tete de liste
// retourne la tete de liste
node_t *list_insert(node_t *head, void *data);

// creation et ajout d'un noeud en queue de liste
// retourne la tete de liste
node_t *list_append(node_t *head, void *data);

// suppression de la premiere instance d'une
// donnee dans la liste, retourne la tete de liste
node_t *list_remove(node_t *head, void *data);

// suppression de la tete de liste
// retourne la nouvelle tete de liste
node_t *list_headRemove(node_t *head);

// destruction d'une liste
// (La liberation des donnees n'est pas prise en charge)
void list_destroy(node_t *head);