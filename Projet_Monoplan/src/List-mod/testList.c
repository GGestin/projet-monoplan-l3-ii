#include "testList.h"

int runTestList()
{
	printf("\n<-------------------->");
	printf("\n<- BEGIN testList.c ->");
	printf("\n<-------------------->\n");

	int ret = 0;
	ret += test_list_create();
	ret += test_list_get_data();
	ret += test_list_set_data();
	ret += test_list_next();
	ret += test_list_insert();
	ret += test_list_append();
	ret += test_list_remove();
	ret += test_list_headRemove();
	ret += test_list_destroy();
	printf("\n%d : Errors in tests ", ret);
	printf("\n\n<-------------------->");
	printf("\n<-- END testList.c -->");
	printf("\n<-------------------->");
	return ret;
}

int test_list_create()
{
	printf("\n-----> test_list_create()");
	int ret = 0;
	// test if this return NULL
	node_t *list1 = list_create();
	if (list1 != NULL) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_get_data()
{
	printf("\n-----> test_list_get_data()");
	int ret = 0;

	node_t *list1 = list_create();
	node_t *list2 = list_create();

	int val1 = 10;
	list1 = list_append(list1, &val1);

	char val2 = 'G';
	list1 = list_append(list1, &val2);

	int val3 = 42;
	list1 = list_append(list1, &val3);

	if (*(int*)list_get_data(list1) != 10) ret++;
	if (*(char*)list_get_data(list_next(list1)) != 'G') ret++;
	if (*(int*)list_get_data(list_next(list_next(list1))) != val3) ret++;
	if (list_get_data(list2) != NULL) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_set_data()
{
	printf("\n-----> test_list_set_data()");
	int ret = 0;

	node_t *list1 = list_create();
	node_t *list2 = list_create();

	int val1 = 10;
	int new_val1 = 1;
	list1 = list_append(list1, &val1);

	char val2 = 'G';
	char new_val2 = 'P';
	list1 = list_append(list1, &val2);

	int val3 = 42;
	int new_val3 = 24;
	list1 = list_append(list1, &val3);

	list_set_data(list1, &new_val1);
	list_set_data(list_next(list1), &new_val2);
	list_set_data(list_next(list_next(list1)), &new_val3);

	if (*(int*)list_get_data(list1) != 1) ret++;
	if (*(char*)list_get_data(list_next(list1)) != 'P') ret++;
	if (*(int*)list_get_data(list_next(list_next(list1))) != new_val3) ret++;
	if (list_get_data(list2) != NULL) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_next()
{
	printf("\n-----> test_list_next()   ");
	int ret = 0;

	node_t *list1 = list_create();
	node_t *list2 = list_create();

	int val1 = 10;
	list1 = list_append(list1, &val1);

	char val2 = 'G';
	list1 = list_append(list1, &val2);

	int val3 = 42;
	list1 = list_append(list1, &val3);

	if (*(int*)list_get_data(list1) != 10) ret++;
	if (*(char*)list_get_data(list_next(list1)) != 'G') ret++;
	if (*(int*)list_get_data(list_next(list_next(list1))) != val3) ret++;
	if (list_next(list2) != NULL) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_insert()
{
	printf("\n-----> test_list_insert()");

	int ret = 0;

	node_t *list1 = list_create();

	int val1 = 10;
	list1 = list_insert(list1, &val1);

	char val2 = 'G';
	list1 = list_insert(list1, &val2);

	int val3 = 42;
	list1 = list_insert(list1, &val3);

	if (*(int*)list_get_data(list1) != val3) ret++;
	if (*(char*)list_get_data(list_next(list1)) != 'G') ret++;
	if (*(int*)list_get_data(list_next(list_next(list1))) != 10) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_append()
{
	printf("\n-----> test_list_append()");
	int ret = 0;

	node_t *list1 = list_create();
	node_t *list2 = list_create();

	int val1 = 10;
	list1 = list_append(list1, &val1);

	char val2 = 'G';
	list1 = list_append(list1, &val2);

	int val3 = 42;
	list1 = list_append(list1, &val3);

	if (*(int*)list_get_data(list1) != 10) ret++;
	if (*(char*)list_get_data(list_next(list1)) != 'G') ret++;
	if (*(int*)list_get_data(list_next(list_next(list1))) != val3) ret++;
	if (list_next(list2) != NULL) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_remove()
{
	printf("\n-----> test_list_remove()");
	int ret = 0;

	node_t *list1 = list_create();
	node_t *list2 = list_create();

	int val1 = 10;
	list1 = list_append(list1, &val1);

	char val2 = 'G';
	list1 = list_append(list1, &val2);

	int val3 = 42;
	list1 = list_append(list1, &val3);

	list1 = list_remove(list1, NULL);
	if (*(int*)list_get_data(list1) != 10) ret++;

	list1 = list_remove(list1, &val1);
	if (*(char*)list_get_data(list1) != val2) ret++;
	
	list1 = list_remove(list1, &val3);
	if (*(char*)list_get_data(list1) != val2) ret++;

	if (list_remove(list2, NULL) != NULL) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_headRemove()
{
	printf("\n-----> test_list_headRemove()");
	int ret = 0;

	node_t *list1 = list_create();
	node_t *list2 = list_create();

	int val1 = 10;
	list1 = list_append(list1, &val1);

	char val2 = 'G';
	list1 = list_append(list1, &val2);

	int val3 = 42;
	list1 = list_append(list1, &val3);

	if (*(int*)list_get_data(list1) != 10) ret++;
	list1 = list_headRemove(list1);

	if (*(char*)list_get_data(list1) != 'G') ret++;
	list1 = list_headRemove(list1);

	if (*(int*)list_get_data(list1) != val3) ret++;
	list1 = list_headRemove(list1);

	if (list_get_data(list1) != NULL) ret++;

	if (list_headRemove(list2) != NULL) ret++;
	printf("\t-> %d Errors", ret);
	return ret;
}

int test_list_destroy()
{
	printf("\n-----> test_list_destroy()");
	int ret = 0;

	node_t *list1 = list_create();
	node_t *list2 = list_create();

	int val1 = 10;
	list1 = list_append(list1, &val1);

	char val2 = 'G';
	list1 = list_append(list1, &val2);

	int val3 = 42;
	list1 = list_append(list1, &val3);

	list_destroy(list1);
	if (!list1) ret++;

	list_destroy(list2);
	if (list2 != NULL) ret++;

	printf("\t-> %d Errors", ret);
	return ret;
}
