#include <stdio.h>
#include <stdlib.h>

#include "list.h"

int runTestList();
int test_list_create(); 
int test_list_get_data();
int test_list_set_data();
int test_list_next();
int test_list_insert();
int test_list_append();
int test_list_remove();
int test_list_headRemove();
int test_list_destroy();
