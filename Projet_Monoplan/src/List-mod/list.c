#include "list.h"

// creation d'une nouvelle liste vide
node_t *list_create(void)
{
	node_t *head = NULL;
	return head;
}

void *list_get_data(node_t *node)
{
	if (node == NULL)
		return NULL;

	return node->data_node;
}

// lire ou ecrire la donnee d'un noeud
void list_set_data(node_t *node, void *data)
{
	if (node == NULL)
		return;

	node->data_node = data;
}

// Obtenir le noeud suivant
node_t *list_next(node_t *node)
{
	if (node == NULL)
		return NULL;

	return node->next_node;
}

// Creation et insertion d'un noeud en tete de liste
node_t *list_insert(node_t *head, void *data)
{
	// test malloc
	node_t *new_node;
	if ((new_node = (node_t *)malloc(sizeof(node_t))) == NULL)
		return NULL;

	list_set_data(new_node, data);
	new_node->next_node = head;
	return new_node;
}

// Creation et ajout d'un noeud en queue de liste
// retourne la tete de liste
node_t *list_append(node_t *head, void *data)
{
	// test malloc
	node_t *new_node;
	if ((new_node = (node_t *)malloc(sizeof(node_t))) == NULL)
		return NULL;

	list_set_data(new_node, data);
	new_node->next_node = NULL;

	if (head == NULL)
		return new_node;

	node_t *p = head;
	while (list_next(p) != NULL) p = list_next(p);

	p->next_node = new_node;
	return head;
}

// Suppression de la premiere instance d'une
// retourne la tete de liste
node_t *list_remove(node_t *head, void *data)
{
	if (head != NULL)
	{
		node_t *tmp, *p;
		if (list_get_data(head) == data)
			return list_headRemove(head);

		else
		{
			tmp = head;
			while ((list_next(tmp) != NULL) && (list_next(tmp) != data))
				tmp = list_next(tmp);

			if (list_next(tmp) != NULL)
			{
				p = list_next(tmp);
				tmp->next_node = list_next(list_next(tmp));
				free(p);
				p = NULL;
			}
		}
	}
	return head;
}

// Suppression de la tete de liste
// donnee dans la liste, retourne la tete de liste
node_t *list_headRemove(node_t *head)
{
	if (head == NULL)
		return NULL;

	node_t *new_node = list_next(head);
	free(head);
	head = NULL;
	return new_node;
}

// Retourne la nouvelle tete de liste
void list_destroy(node_t *head)
{
	while ((head = (node_t*)list_headRemove(head)) != NULL);
}
