# **# Projet Monoplan[.](https://pagesperso.univ-brest.fr/~rubini/)** 
This Gitlab will serve me for versioning and sharing in this Bachelor Project coded in C.

### [Gaëtan Gestin](https://gitlab.com/GGestin/projet-monoplan-l3-ii) 
> *Projet de Programmation C avancée* <br> Licence 3 Ingénierie Informatique   <br> 2020/2021 

# #0 Table of content
### #1 [How to compile](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/tree/master#1-how-to-compile)
### #2 [Code Architecture](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/tree/master#2-code-architecture-1)
### #3 [Documents (French)](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/tree/master#3-documents-french-1)
### #4 [Weekly Reports (French)](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/tree/master#4-weekly-reports-french-1)

# #1 How to compile
Go to the root folder of the project and use 
1. To compile the main program :<br> `make` 
1. To launch the **main** program :<br>
```make```
1. To launch the **test** program :<br>
```make tests```
1. To clean all compiled files :<br>
```make clean```

> **Note :**
<br>`make build` is used to compile if `make` doesn't work
<br>`make gdbt` is used to debug

# #2 Code Architecture
```
Project_Monoplan
├── bin
│   └── tests
├── exe
│   ├── GGmonoplan
│   └── test_GGmonoplan
├── makefile
├── README.md
└── src
    ├── Cell-mod
    │   ├── cell.c
    │   ├── cell.h
    │   ├── testCell.c
    │   └── testCell.h
    ├── List-mod
    │   ├── list.c
    │   ├── list.h
    │   ├── testList.c
    │   └── testList.h
    ├── main.c
    ├── main.h
    ├── mainTest.c
    ├── mainTest.h
    └── stack-lib
        ├── stack.h
        ├── stack.o
        └── testStack.c

```
N°  | Folder | Description
--  | ------ | -----------
*1*  |`bin/`  | It contains compiled files (.o)
*2*  |`bin/tests/`  | It contains compiled files for tests only
*3*  |`exe/`  | It contains executable files (**main** and **test** programs) 
*4*  |`src/`  | It contains all the **source code**
*5*  |`src/List-mod/`  | The List Module which represent simple linked chains
*6*  |`src/Cell-mod/`  | The Cell Module which represent a simple cell for the project
*7*  |`src/main.c`  | The main routine for the program
*8*  |`src/mainTest.c`  | The main routine for the automated tests
*9*  |`src/stack-lib/`  | A library about stacks

# #3 Documents (French)

* [Présentation du projet](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/tableurPresentation.pdf)
* [Module "Liste chaînée"](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/tableurListe.pdf)
* [Module "Evaluation d'une cellule"](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/tableurEval.pdf)
* [Module "Graphe des cellules"](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/tableurGraphe.pdf)
* [Module "IHM"](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/tableurIHM.pdf)
* * [Introduction à GTK](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/Introduction_a_gtk_gtk3.pdf)
* *Module "Intégration" TODO*

# #4 Weekly Reports (French)

1. [Week 46 report](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/week46.txt)
1. [Week 47 report](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/week47.txt)
1. [Week 48 report](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/week48.txt)
1. [Week 49 report](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/week49.txt)
1. [Week 50 report](https://gitlab.com/GGestin/projet-monoplan-l3-ii/-/blob/master/Documentation/week50.txt)
